import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  name:string = "";
  email:string = "";
  password:string = "";
  // confirm:string = "";

  constructor(private api: ApiService) { }

  ngOnInit() {
  }

  register(){
    this.api.register({
        'name' : this.name,
        'email' : this.email,
        'password' : this.password
    });
  }
  
}
