import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

declare var jQuery:any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  data:object[];
  notification: string = "";

  name: string = "";
  address: string = "";
  email: string = "";

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.validateUser();
    this.notification = localStorage.getItem("message");

    jQuery("#dialog").dialog({
      autoOpen : false,
      show:{
        effect : "blind",
        duration : 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      }
    });
  }
 
  add(){
    this.api.addData({
      'name' : this.name,
      'address' : this.address,
      'email' : this.email
    });

    this.name = "";
    this.address = "";
    this.email = "";
  }

  remove(index:number){
    this.api.deleteData({
      'id' : index,
    });
  }

  reset(){
    this.name = "";
    this.address = "";
    this.email = "";
  }

  open(){
    jQuery("#dialog").dialog("open");
  }
}
