<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Vendor;

class VendorController extends Controller
{
    public function index(){
        $list = Vendor::get();

        return response()->json($list, 200);
    }

    public function add(){
        $list = Vendor::get();

        return response()->json($list, 200);
    }

    public function remove(){
        $list = Vendor::get();

        return response()->json($list, 200);
    }
}
