<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Product;

class ProductController extends Controller
{
    public function index(){
        $list = Product::get();

        return response()->json($list, 200);
    }

    public function getByBrand($id){
        $list = Product::find()->where('brand_id','=', $id)->get();

        return response()->json($list, 200);
    }

}
