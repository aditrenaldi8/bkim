<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\User;

class AuthenticationController extends Controller
{
        public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }

    public function register(Request $request){
        try{
            $user = new User;
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->address = $request->input('address');
            $user->phone_number = $request->input('phone_number');
            $user->password = bcrypt($request->input('password'));
            $user->save();

            $token = JWTAuth::fromUser($user);
            return response()->json(["message"=>"Successfully Create User", "token"=>$token], 200);
        }catch(\Exception $e){
            return response()->json(["message"=>"Failed to Create User : " + $e], 500);
        }
    }
}
