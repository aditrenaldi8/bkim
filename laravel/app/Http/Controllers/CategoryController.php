<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Category;

class CategoryController extends Controller
{
    public function index(){
        $list = Category::get()->toTree();

        return response()->json($list, 200);
    }

    public function add(Request $request){

        DB::beginTransaction();

        try{
            $this->validate($request,[
                'category_name' => 'required'
            ]);

            $newData = new Category;
            $newData->category_name = $request->input('category_name');
            $newData->parent_id = $request->input('parent_id');
            $newData->save();

            DB::commit();
            return response()->json(["message"=>"Success"], 200);

        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }


    }

    public function remove(){

    }
}
