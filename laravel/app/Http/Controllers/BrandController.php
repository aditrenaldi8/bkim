<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Brand;

class BrandController extends Controller
{
    public function index(){
        $list = Brand::get();

        return response()->json($list, 200);
    }

    public function add(Request $request){

        DB::beginTransaction();

        try{
            $this->validate($request,[
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            
            $name = $request->input('name');

            $image = $request->file('image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $input['imagename']);

            $newData = new Brand;
            $newData->brand_name = $name;
            $newData->logo = $input['imagename'];
            $newData->save();

            DB::commit();
            return response()->json(["message"=>"Success"], 200);


        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }   

    }

    public function remove(Request $request){
        DB::beginTransaction();

        try{
            $this->validate($request,[
                'id' => 'required',
            ]);

            $data = Brand::find((integer)$request->input("id"));
            if(empty($data)){
                return response()->json(["message"=>"User Not Found"], 404);
            }

            $data->delete();
            DB::commit();

            return response()->json(["message"=>"Success"], 200);

        }catch(\Exception $e){
             DB::rollBack();
            return response()->json(["message"=> $e->getMessage], 500);
        }
    }
}
