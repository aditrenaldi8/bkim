<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix'=> 'user'], function(){
   Route::post('register','AuthenticationController@register');
   Route::post('login','AuthenticationController@authenticate'); 
});

Route::group(['prefix'=> 'products'], function(){
   Route::get('','ProductController@index');
   Route::get('brand/{id}','ProductController@getByBrand'); 
});

Route::group(['prefix'=> 'brands'], function(){
   Route::get('','BrandController@index');
   Route::post('add','BrandController@add'); 
   Route::post('remove','BrandController@remove'); 
});

Route::group(['prefix'=> 'categories'], function(){
   Route::get('','CategoryController@index');
   Route::post('add','CategoryController@add');
   Route::post('remove','CategoryController@remove');  
});

Route::group(['prefix'=> 'vendors'], function(){
   Route::get('','VendorController@index');
   Route::post('add','VendorController@add'); 
   Route::post('remove','VendorController@remove');
});